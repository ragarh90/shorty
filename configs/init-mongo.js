/**
 * Note: The ideal configuration for this file is to avoid any hardcoded name for the databases
 * however to do that i required to pass extra ENV files to the MongoDB docker image which was falling outisde the scope of this excercise.
 * 
 * In order to have a version more close for a production release we can drop the use of the MongoDB image and use any cloud hosted service like AWS
 * which will provide a better perfomance and security
*/
// Create Prod database
use shortly_prod
//Inserting dummy collection to ensure database is created
db.dummy.insert({name: "dummy"})
db.createUser({
  user: "shortly",
  pwd:  "shortly",
  roles:[
    {
      role: "readWrite",
      db: "shortly_prod"
    }
  ]
})

// Create Dev database
use shortly_dev
//Inserting dummy collection to ensure database is created
db.dummy.insert({name: "dummy"})
db.createUser({
  user: "shortly",
  pwd:  "shortly",
  roles:[
    {
      role: "readWrite",
      db: "shortly_dev"
    }
  ]
})

// Create Test database
use shortly_test
//Inserting dummy collection to ensure database is created
db.dummy.insert({name: "dummy"})
db.createUser({
  user: "shortly",
  pwd:  "shortly",
  roles:[
    {
      role: "readWrite",
      db: "shortly_test"
    }
  ]
})